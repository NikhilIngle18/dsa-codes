import java.util.ArrayList;
import java.util.Collections;

class Solution {
    // Function to find the leaders in the array.
    static ArrayList<Integer> leaders(int arr[], int n) {
        // Your code here
        ArrayList<Integer> al = new ArrayList<Integer>();

        int max = arr[n - 1];
        al.add(max);

        for (int i = n - 2; i >= 0; i--) {
            if (arr[i] >= max) {
                max = arr[i];
                al.add(max);
            }
        }
        Collections.reverse(al);
        return al;
    }

    public static void main(String[] args) {
        int arr[] = new int[] { 1, 5, 1, 7 };
        ArrayList<Integer> al = leaders(arr, arr.length);

        System.out.println(al);
    }
}
