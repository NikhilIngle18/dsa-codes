import java.util.HashMap;

class Solution {

    static int getPairsCount(int[] arr, int n, int k) {
        // code here
        // int count =0;
        // for(int i=0;i<n;i++){
        // for(int j=i+1;j<n;j++){
        // if(arr[i]+arr[j]==k){
        // count ++;
        // }
        // }
        // }
        // return count;
        // int count=0;
        // Arrays.sort(arr);
        // int right=n-1,left=0;

        // while(left<right){
        // if(arr[left]+arr[right]==k){
        // count++;
        // left++;

        // }else if(arr[left]+arr[right]>k){
        // right--;
        // }else if(arr[left]+arr[right]<k){
        // left++;
        // }
        // }
        // return count;
        HashMap<Integer, Integer> hm = new HashMap<>();
        int count = 0;
        for (int i = 0; i < n; i++) {
            if (hm.containsKey(k - arr[i])) {
                count = count + hm.get(k - arr[i]);
            }
            if (hm.get(arr[i]) == null) {
                hm.put(arr[i], 1);
            } else {
                int f = hm.get(arr[i]);
                hm.put(arr[i], f + 1);
            }
        }
        return count;

    }

    public static void main(String[] args) {
        int arr[] = new int[] { 1, 5, 1, 7 };
        System.err.println(" " + getPairsCount(arr, arr.length, 6));
    }
}
