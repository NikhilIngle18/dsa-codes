class Sol {
    public static int matSearch(int mat[][], int N, int M, int X) {
        int r = 0;
        int c = mat[0].length - 1;
        while (r < mat.length && c >= 0) {
            if (mat[r][c] == X) {
                return 1;
            } else if (mat[r][c] > X) {
                c--;

            } else {
                r++;
            }
        }
        return 0;

    }

}