class Solution {
    public static boolean searchMatrix(int[][] matrix, int target) {
        int idx = 0;
        int n = matrix[0].length - 1;
        for (int i = 0; i < matrix.length; i++) {
            if (matrix[i][n] < target) {
                idx = i;
                break;
            }
        }
        for (int i = idx; idx < matrix.length; idx++) {
            for (int j = 0; j < matrix[0].length; j++) {
                if (target == matrix[idx][j]) {
                    return true;
                }
            }
        }
        return false;
    }

    public static void main(String[] args) {
        int matrix[][] = { { 1, 3, 5, 7 }, { 10, 11, 16, 20 }, { 23, 30, 34, 60 } };
        System.out.println(searchMatrix(matrix, 13));

    }
}