import java.util.HashMap;

class Solution {
    public int[] twoSum(int[] arr, int target) {
        int n = arr.length;
        int ans[] = new int[2];
        // ArrayLis<Integer> al = new ArrayList<Integer>();
        HashMap<Integer, Integer> hm = new HashMap<>();

        for (int i = 0; i < n; i++) {
            if (hm.containsKey(target - arr[i])) {
                ans[1] = i;
                ans[0] = hm.get(target - arr[i]);
            } else {
                hm.put(arr[i], i);
            }

        }
        return ans;
    }

}
