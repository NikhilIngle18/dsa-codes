class P {
    public static int removeElement(int[] arr, int val) {
        int k = 0;
        for (int i = 0; i < arr.length; i++) {

            if (arr[i] != val) {
                arr[k] = arr[i];
                k++;
            }
        }
        return k;
    }

    public static void main(String arg[]) {
        int arr[] = new int[] { 0, 1, 2, 2, 3, 0, 4, 2 };
        int k = removeElement(arr, 2);
        for (int i = 0; i < k; i++) {
            System.out.print(arr[i]);
        }
    }
}