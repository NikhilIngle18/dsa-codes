class Compute {
    static long[] minAnd2ndMin(long arr[], long n) {
        /*
         * long min=Integer.MAX_VALUE;
         * long smin=Integer.MAX_VALUE;
         * long ans[] = new long[2];
         * 
         * for(int i=0;i<n;i++){
         * if(arr[i]<min){
         * smin=min;
         * min=arr[i];
         * }else{
         * if(arr[i]<smin && arr[i]!=min){
         * smin=arr[i];
         * 
         * }
         * }
         * }
         * if(smin==Integer.MAX_VALUE)
         * {
         * 
         * ans[0]=-1;
         * return ans;
         * 
         * 
         * }else{
         * ans[0]=min;
         * ans[1]=smin;
         * return ans;
         * }
         */

        long max = Long.MAX_VALUE;
        Long smallest = max;
        Long secSmallest = max;

        for (int i = 0; i < n; i++) {

            if (arr[i] < smallest)
                smallest = arr[i];
        }

        for (int i = 0; i < n; i++) {

            if (arr[i] < secSmallest && arr[i] > smallest)
                secSmallest = arr[i];
        }

        if (smallest != max && secSmallest != max)
            return new long[] { smallest, secSmallest };
        else
            return new long[] { -1 };
    }

    public static void main(String[] args) {

        long arr[] = { 1, 2, 3, 2, 1, 0 };
        long ans[] = minAnd2ndMin(arr, arr.length);

        System.out.println(java.util.Arrays.toString(ans));
    }
}
