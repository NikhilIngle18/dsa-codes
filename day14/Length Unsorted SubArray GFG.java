import java.util.Arrays;

class Solve {
    static int[] printUnsorted(int[] arr, int n) {

        int[] a = new int[n];
        int[] ans = new int[2];
        for (int i = 0; i < n; i++) {
            a[i] = arr[i];
        }
        Arrays.sort(a);
        for (int i = 0; i < n; i++) {
            if (a[i] != arr[i]) {
                ans[0] = i;
                break;
            }
        }
        for (int i = n - 1; i >= 0; i--) {
            if (a[i] != arr[i]) {
                ans[1] = i;
                break;
            }
        }
        return ans;

    }

    public static void main(String[] args) {
        int Arr[] = { 10, 12, 20, 30, 25, 40, 32, 31, 35, 50, 60 };

        int ans[] = printUnsorted(Arr, Arr.length);
        System.out.println(Arrays.toString(ans));

    }
}