import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

class Solution {
    public static int[] intersection(int[] nums1, int[] nums2) {
        Set<Integer> s = new HashSet<Integer>();
        ArrayList<Integer> al = new ArrayList<>();

        for (int i = 0; i < nums1.length; i++) {
            s.add(nums1[i]);
        }

        for (int i = 0; i < nums2.length; i++) {
            if (s.contains(nums2[i])) {
                al.add(nums2[i]);
                s.remove(nums2[i]);
            }
        }

        int arr[] = new int[al.size()];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = al.get(i);
        }
        return arr;
    }

    public static void main(String[] args) {
        int nums1[] = { 4, 9, 5 };
        int nums2[] = { 9, 4, 9, 8, 4 };

        int arr[] = intersection(nums1, nums2);

        System.out.println(Arrays.toString(arr));

    }
}