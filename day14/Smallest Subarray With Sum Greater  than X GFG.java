
class Solution {

    public static int smallestSubWithSum(int a[], int n, int x) {
        // Your code goes here

        int sum = 0;
        int start = 0;
        int minimumLength = Integer.MAX_VALUE;
        for (int i = 0; i < n; i++) {
            sum += a[i];

            while (sum > x) {
                minimumLength = Math.min(minimumLength, (i + 1) - start);
                sum -= a[start];
                start++;
            }
        }
        return (minimumLength == Integer.MAX_VALUE ? 0 : minimumLength);

    }

    public static void main(String[] args) {
        int arr[] = { 1, 4, 45, 6, 0, 19 };
        int x = 51;
        System.out.println(smallestSubWithSum(arr, arr.length, x));
    }
}
