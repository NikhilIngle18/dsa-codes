import java.util.Arrays;

class Solution {
    static int[] runningSum(int[] nums) {
        for (int i = 1; i < nums.length; i++) {
            nums[i] = nums[i - 1] + nums[i];
        }
        return nums;
    }

    public static void main(String[] args) {
        int[] nums = new int[] { 1, 2, 3, 4, 5 };
        nums = runningSum(nums);
        System.out.println(Arrays.toString(nums));

    }
}