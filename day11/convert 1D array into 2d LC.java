import java.util.Arrays;

class Solution {
    static int[][] construct2DArray(int[] original, int m, int n) {
        int row = 0;
        int column = 0;

        int a[][] = new int[m][n];
        if (m * n != original.length) {
            return new int[0][0];
        }

        for (int i = 0; i < original.length; i++) {
            a[row][column] = original[i];

            if (column < (n - 1)) {
                column++;
            } else {
                row++;
                column = 0;
            }
        }
        return a;
    }

    public static void main(String[] args) {
        int[] original = new int[] { 1, 2, 3 };
        int[][] a = construct2DArray(original, 1, 3);

    }
}