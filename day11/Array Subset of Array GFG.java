import java.util.Arrays;

class Compute {
    static String isSubset(long a1[], long a2[], long n, long m) {
        Arrays.sort(a1);
        Arrays.sort(a2);

        int i = 0, j = 0;

        while (i < n && j < m) {

            if (a1[i] == a2[j]) {
                i++;
                j++;
            } else if (a1[i] < a2[j]) {
                i++;
            }

            else {
                return "No";
            }
        }

        if (j == m)
            return "Yes";

        else
            return "No";

    }

    public static void main(String[] args) {
        long a1[] = new long[] { 11, 7, 1, 13, 21, 3, 7, 3 };
        long a2[] = new long[] { 11, 3, 7, 1, 7 };
        System.out.println(isSubset(a1, a2, a1.length, a2.length));

    }
}