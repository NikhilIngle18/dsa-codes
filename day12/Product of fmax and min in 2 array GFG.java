class Solution {

    static long find_multiplication(int arr[], int brr[], int n, int m) {
        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < m; i++) {
            if (brr[i] < min) {
                min = brr[i];
            }
        }
        for (int i = 0; i < n; i++) {
            if (arr[i] > max) {
                max = arr[i];
            }
        }

        return min * max;
    }

    public static void main(String[] args) {
        int arr[] = new int[] { 1, 2, 3, 4, 5, 6 };
        int brr[] = { 9, -1, 0, 4, 5, 4 };
        System.out.println(find_multiplication(arr, brr, arr.length, brr.length));
    }

}
