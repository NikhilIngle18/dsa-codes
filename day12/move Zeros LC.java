import java.util.Arrays;

class Solution {
    static void moveZeroes(int[] nums) {
        int count = 0;
        int j = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == 0) {
                count++;
            } else {
                nums[j] = nums[i];
                j++;
            }
        }
        int k = nums.length - 1;
        while (count != 0) {
            nums[k] = 0;
            count--;
            k--;
        }
        System.out.println(Arrays.toString(nums));

    }

    public static void main(String[] args) {
        int nums[] = new int[] { 0, 1, 0, 3, 12 };
        moveZeroes(nums);
    }
}