class pair {
    long first, second;

    public pair(long first, long second) {
        this.first = first;
        this.second = second;
    }
}

class Solution {

    static pair indexes(long v[], long x) {
        // Your code goes here
        int n = v.length;

        // ********APPROACH 1*********************************
        // long left = -1;
        // long right = -1;
        // int n = v.length;
        // for(int i = 0; i < n; i++) {
        // if(v[i] == x) {
        // if(left == -1) {
        // left = i;
        // }
        // right = i;
        // }
        // }

        // if(left == -1 && right == -1) {
        // return new pair(-1, -1);
        // // } else {
        // return new pair(left, right);
        // }***********************************************

        int i = 0;
        int j = n - 1;
        while (i < j) {
            if (v[i] != x) {
                i++;

            }
            if (v[j] != x) {
                j--;
            }
            if (v[i] == x && v[j] == x) {
                return new pair(i, j);
            }
        }
        return new pair(-1, -1);

    }

    public static void main(String[] args) {
        long nums[] = new long[] { 0, 1, 0, 5, 5, 5, 5, 5, 3, 12 };
        indexes(nums, 5);
    }
}