package leetcode;

import java.util.Arrays;

class Sn {
    public static int[] findMissingAndRepeatedValues(int[][] grid) {
        int n = grid.length;
        int arr1[] = new int[n * n];
        int arr[] = new int[n * n];
        int k = 0;
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                arr[k] = grid[i][j];// filled elements of grid[][] to arr
                k++;
            }
        }

        int ans[] = new int[2];
        int missing = -1;
        int repeting = -1;
        for (int i = 0; i < n * n; i++) {

            arr1[arr[i] - 1] = arr1[arr[i] - 1] + 1; // taking elenents of arr[] as index for arr1 and incrementing
                                                     // value if already present
            if (arr1[arr[i] - 1] > 1)
                repeting = arr[i];
        }
        for (int i = 0; i < n * n; i++) {
            if (arr1[i] == 0) {
                missing = i + 1;
                break;
            }
        }
        ans[0] = repeting;
        ans[1] = missing;
        return ans;

    }

    public static void main(String[] args) {
        int[][] grid = { { 9, 1, 7 }, { 8, 9, 2 }, { 3, 4, 6 } };
        System.out.println(Arrays.toString(findMissingAndRepeatedValues(grid)));

    }
}
