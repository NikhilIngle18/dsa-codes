class Solution {

    // a: input array
    // n: size of array
    // Function to find equilibrium point in the array.
    static int equilibriumPoint(long a[], int n) {

        // Your code here
        long frontSum[] = new long[n];
        long backSum[] = new long[n];
        frontSum[0] = a[0];

        for (int i = 1; i < n; i++) {
            frontSum[i] = frontSum[i - 1] + a[i];
        }

        backSum[n - 1] = a[n - 1];
        for (int i = n - 2; i >= 0; i--) {
            backSum[i] = backSum[i + 1] + a[i];
        }

        for (int i = 0; i < n; i++) {
            if (frontSum[i] == backSum[i]) {
                return i + 1;
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        long a[] = new long[] { 1, 2, 3, 2, 2 };
        System.out.println(equilibriumPoint(a, a.length));
    }

}