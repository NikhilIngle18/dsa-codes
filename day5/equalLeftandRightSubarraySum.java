class Solution {
    static int equalSum(int[] a, int n) {
        // Write your code here
        int frontSum[] = new int[n];
        int backSum[] = new int[n];
        frontSum[0] = a[0];

        for (int i = 1; i < n; i++) {
            frontSum[i] = frontSum[i - 1] + a[i];
        }

        backSum[n - 1] = a[n - 1];
        for (int i = n - 2; i >= 0; i--) {
            backSum[i] = backSum[i + 1] + a[i];
        }

        for (int i = 0; i < n; i++) {
            if (frontSum[i] == backSum[i]) {
                return i + 1;
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        int a[] = new int[] { 1, 2, 3, 2, 2 };
        System.out.println(equalSum(a, a.length));
    }
}
