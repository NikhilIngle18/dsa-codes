class Solution {
    public static int pivotIndex(int[] arr) {
        int n = arr.length;
        for (int i = 0; i < n; i++) {
            int leftSum = 0;
            int rightSum = 0;
            for (int j = 0; j < i; j++) {
                leftSum = leftSum + arr[j];
            }
            for (int k = i + 1; k < n; k++) {
                rightSum = rightSum + arr[k];
            }
            if (leftSum == rightSum) {
                return i;
            }
        }
        return -1;

        /*
         * ANOTHER APPROACH
         * 
         * 
         * int n=nums.length;
         * int suff[]=new int[n];
         * int pref[]=new int[n];
         * suff[0]=nums[0];
         * pref[n-1]=nums[n-1];
         * int k=0;
         * for(int i=1;i<n;i++){
         * k=n-i-1;
         * suff[i]=suff[i-1]+nums[i];
         * pref[k]=pref[k+1]+nums[k];
         * }
         * for(int x=0;x<nums.length;x++){
         * if(suff[x]==pref[x]){
         * return x;
         * }
         * }
         * return -1;
         * 
         */
    }

    public static void main(String[] args) {
        int[] arr = new int[] { 1, 7, 3, 6, 5, 6 };
        System.out.println(pivotIndex(arr));
    }
}