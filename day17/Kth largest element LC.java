import java.util.Arrays;

class Solution {
    public static int findKthLargest(int[] nums, int k) {
        Arrays.sort(nums);
        return nums[nums.length - k];
    }

    public static void main(String[] args) {

        int[] nums = { 3, 2, 4, 5, 5, 77, 5, 43, 6, 98 };
        int k = 4;
        System.out.println(findKthLargest(nums, k));

    }

}