import java.util.Arrays;

class Solution {
    public static int kthSmallest(int[] arr, int l, int r, int k) {

        Arrays.sort(arr);
        return arr[k - 1];
    }

    public static void main(String[] args) {

        int[] nums = { 3, 2, 4, 5, 5, 77, 5, 43, 6, 98 };
        int k = 4;
        int l = 2;
        int r = 7;
        System.out.println(kthSmallest(nums, l, r, k));

    }
}
