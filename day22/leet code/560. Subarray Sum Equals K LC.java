import java.util.*;

class Solution {
    public int subarraySum(int[] Arr, int k) {

        // int ans = 0;
        // for(int i = 0; i < Arr.length; i++){
        // int sum = 0;
        // for(int j = i; j <Arr.length; j++){
        // sum += Arr[j];
        // if(sum == k){
        // ans++;
        // }
        // }
        // }
        // return ans;
        int N = Arr.length;

        HashMap<Integer, Integer> map = new HashMap<>();
        map.put(0, 1); // (sum,count)

        int sum = 0;
        int ans = 0;

        for (int i = 0; i < N; i++) {
            sum += Arr[i];

            if (map.containsKey(sum - k)) {

                ans += map.get(sum - k);
            }
            if (map.containsKey(sum)) {
                map.put(sum, map.get(sum) + 1);
            } else {
                map.put(sum, 1);
            }
        }
        return ans;

    }
}