package gfg;

import java.util.*;;

class Solution {
    // Function to find maximum of each subarray of size k.
    static ArrayList<Integer> max_of_subarrays(int arr[], int n, int k) {
        // Your code here
        ArrayList al = new ArrayList<>();
        for (int i = 0; i <= n - k; i++) {
            int max = Integer.MIN_VALUE;
            for (int j = i; j < i + k; j++) {
                max = Math.max(max, arr[j]);
            }
            al.add(max);
        }
        return al;

    }

    public static void main(String[] args) {
        int N = 9, K = 3;
        int arr[] = { 1, 2, 3, 1, 4, 5, 2, 3, 6, };
        ArrayList al = max_of_subarrays(arr, N, K);
        System.out.println(al);

    }
}