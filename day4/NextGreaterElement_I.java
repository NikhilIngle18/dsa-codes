
class Solution {

    static int[] nextGreaterElement(int[] nums1, int[] nums2) {
        int n = nums1.length;
        int ans[] = new int[n];
        java.util.Arrays.fill(ans, -1);
        int k = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < nums2.length; j++) {
                if (nums1[i] == nums2[j]) {
                    for (int x = j + 1; x < nums2.length; x++) {
                        if (nums2[j] < nums2[x]) {
                            ans[i] = nums2[x];
                            break;

                        }

                    }

                }

            }

        }
        return ans;
    }

    public static void main(String[] args) {
        int[] nums1 = new int[] { 4, 1, 2 };
        int[] nums2 = new int[] { 1, 2, 3, 4 };
        int[] ans = nextGreaterElement(nums1, nums2);

        for (int i : ans) {
            System.out.println(i);

        }
    }
}