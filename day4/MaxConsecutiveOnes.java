class Solution {
    static int findMaxConsecutiveOnes(int[] arr) {
        int count = 0, maxcount = 0;

        for (int i = 0; i < arr.length; i++) {
            if (1 == arr[i]) {
                count++;
            } else if (arr[i] != 1) {
                if (count > maxcount) {
                    maxcount = count;
                }
                count = 0;
            }

        }
        if (count > maxcount) {
            maxcount = count;
        }
        return maxcount;
    }

    public static void main(String a[]) {
        int nums[] = new int[] { 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1,0,1,1, };
        System.out.print(findMaxConsecutiveOnes(nums));
    }
}