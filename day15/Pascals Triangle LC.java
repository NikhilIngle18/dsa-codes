import java.util.ArrayList;
import java.util.List;

class Solution {
    public static List<List<Integer>> generate(int numRows) {
        List<List<Integer>> ans = new ArrayList<>();

        for (int i = 0; i < numRows; i++) {
            ArrayList<Integer> al = new ArrayList<>();
            for (int j = 0; j <= i; j++) {
                if (j == 0 || j == i) {
                    al.add(1);
                } else {
                    al.add(ans.get(i - 1).get(j - 1) + ans.get(i - 1).get(j));
                }
            }
            ans.add(al);
        }
        return ans;
    }

    public static void main(String[] args) {
        int numRows = 15;
        List<List<Integer>> ans = generate(numRows);
        System.out.println(ans);

    }
}