
class Solution {
    // Function to sort the array using bubble sort algorithm.
    public static void bubbleSort(int arr[], int n) {
        // code here
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n - i - 1; j++) {
                if (arr[j] > arr[j + 1]) {
                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }

    }

    public static void main(String[] args) {
        int arr[] = { 3, 45, 6, 4, 2, 99, 6, 4, 3 };
        bubbleSort(arr, arr.length);
    }
}