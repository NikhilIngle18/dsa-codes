
class Solution {
    static int minDist(int a[], int n, int x, int y) {
        // code here
        int first = -1;
        int last = -1;
        int result = Integer.MAX_VALUE;
        for (int i = 0; i < n; i++) {
            if (a[i] == x) {
                first = i;
            }
            if (a[i] == y) {
                last = i;
            }
            if (first != -1 && last != -1) {
                int dist = first - last;
                if (dist < 0) {
                    dist = dist * -1;
                }
                result = Math.min(result, dist);
            }
        }
        if (result == Integer.MAX_VALUE) {
            return -1;
        }
        return result;

    }

    public static void main(String[] args) {
        int[] arr = { 1, 2, 3, 2 };
        int distance = minDist(arr, arr.length, 1, 2);
        System.out.println(distance);
    }

}