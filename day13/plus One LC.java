import java.util.Arrays;

class Solution {
    static int[] plusOne(int[] arr) {

        for (int i = arr.length - 1; i >= 0; i--) {

            if (arr[i] < 9) {
                arr[i]++;
                return arr;
            }
            if (arr[i] == 9) {
                arr[i] = 0;
            }
        }
        arr = new int[arr.length + 1];
        arr[0] = 1;
        return arr;
    }

    public static void main(String[] args) {
        int[] arr = { 3, 9, 9 };
        arr = plusOne(arr);
        System.out.println(Arrays.toString(arr));

    }
}