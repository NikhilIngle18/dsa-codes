class Solution {
    public static int missingNumber(int[] nums) {
        int n = nums.length;

        int sum = n * (n + 1) / 2;
        int sum2 = 0;
        for (int i = 0; i < n; i++) {
            sum2 = sum2 + nums[i];
        }
        return sum - sum2;
    }

    public static void main(String[] args) {
        int[] nums = new int[] { 9, 6, 4, 2, 3, 5, 7, 0, 1 };
        System.out.println(missingNumber(nums));
    }
}