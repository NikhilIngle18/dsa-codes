class Solution {
    public static int[] applyOperations(int[] nums) {
        int n = nums.length;

        for (int i = 0; i < n - 1; i++) {
            if (nums[i] == nums[i + 1] && nums[i] != 0) {
                nums[i] = nums[i] * 2;
                nums[i + 1] = 0;

            }
        }

        int j = 0;
        int count = 0;
        for (int i = 0; i < n; i++) {
            if (nums[i] == 0) {
                count++;

            }
            if (nums[i] > 0) {
                nums[j] = nums[i];
                j++;
            }
        }

        int k = n - 1;
        while (count != 0) {
            nums[k] = 0;
            k--;
            count--;

        }

        return nums;

    }

    public static void main(String[] args) {
        int nums[] = new int[] { 1, 0, 2, 0, 0, 1 };
        nums = applyOperations(nums);
        for (int i = 0; i < nums.length; i++) {
            System.out.print(nums[i]);
        }

    }
}