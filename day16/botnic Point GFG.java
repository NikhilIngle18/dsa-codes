class Solution {
    static int findMaximum(int[] arr, int n) {
        // code here

        for (int i = 1; i < n; i++) {
            if (arr[i - 1] > arr[i]) {
                return arr[i - 1];
            }
        }
        return arr[n - 1];
    }

    public static void main(String[] args) {
        int arr[] = { 1, 45, 47, 50, 55, 78, 99, 780 };

        System.out.println(findMaximum(arr, arr.length));

    }
}