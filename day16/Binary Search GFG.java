class Solution {
    static int binarysearch(int arr[], int n, int k) {
        // code here
        int start = 0;
        int end = n - 1;

        while (start <= end) {
            int mid = start + (end - start) / 2;
            if (arr[mid] == k) {
                return mid;
            } else if (k < arr[mid]) {
                end = mid - 1;
            } else {
                start = mid + 1;
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        int arr[] = { 1, 2, 3, 34, 55, 76, 89 };
        int k = 55;
        System.out.println(binarysearch(arr, arr.length, k));
    }
}
