import java.util.ArrayList;

class Solution {
    void rearrange(int arr[], int n) {
        // code here

        // code here
        ArrayList<Integer> Positive = new ArrayList<>();
        ArrayList<Integer> Negative = new ArrayList<>();

        // Find all positive and negative elements
        for (int i = 0; i < n; i++) {
            if (arr[i] >= 0) {
                Positive.add(arr[i]);
            } else {
                Negative.add(arr[i]);
            }
        }

        int i = 0;
        int j = 0;
        int k = 0;
        while (i < Positive.size() && j < Negative.size()) {
            arr[k++] = Positive.get(i++);
            arr[k++] = Negative.get(j++);
        }

        while (i < Positive.size()) {
            arr[k++] = Positive.get(i++);
        }
        while (j < Negative.size()) {
            arr[k++] = Negative.get(j++);
        }

        // ***********************************
        // while(k<arr.length){
        // if(i<Positive.size()){
        // arr[k++]=Positive.get(i++);
        // }

        // if(j<Negative.size()){
        // arr[k++]=Negative.get(j++);
        // }

        // }
    }

}
