class Compute {
    public int findElement(int arr[], int n) {

        // ************TLE******************
        // int i=1;
        // for( i=1;i<n-1;i++){
        // int flag=0,f=0;
        // int j=i-1;
        // int k=i+1;
        // while(j>=0){
        // if(arr[j]>arr[i]){
        // flag=1;
        // break;
        // }
        // j--;
        // }
        // while(k<=n){
        // if(arr[k]<arr[i]){
        // f=1;
        // break;
        // }
        // }

        // if(flag==0&&f==0){
        // return(arr[i]);
        // }

        // }

        // return -1;
        // ********************************* */

        int[] mx = new int[n];
        int[] mi = new int[n];

        int max = 0;
        int min = Integer.MAX_VALUE;

        for (int i = 0; i < n; i++) {
            max = Math.max(arr[i], max);
            mx[i] = max;
        }

        for (int i = n - 1; i >= 0; i--) {
            min = Math.min(arr[i], min);
            mi[i] = min;
        }

        for (int i = 1; i < n - 1; i++) {
            if (mx[i] == mi[i])
                return mx[i];
        }

        return -1;

    }
}
