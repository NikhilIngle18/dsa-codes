import java.util.Arrays;

class Solution {
    static int[] sortedSquares(int[] nums) {
        for (int i = 0; i < nums.length; i++) {
            nums[i] = nums[i] * nums[i];
        }
        Arrays.sort(nums);
        return nums;
    }

    public static void main(String[] args) {
        int nums[] = new int[] { -4, -1, 0, 3, 10 };
        sortedSquares(nums);
    }
}