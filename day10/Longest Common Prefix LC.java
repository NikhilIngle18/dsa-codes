import java.util.Arrays;

class Solution {
    static String longestCommonPrefix(String[] strs) {
        if (strs.length == 1) {
            return strs[0];
        }
        Arrays.sort(strs);
        int count = 0;
        char[] ch1 = strs[0].toCharArray();
        for (int i = 1; i < strs.length; i++) {

            char[] ch2 = strs[i].toCharArray();

            int min = Math.min(ch1.length, ch2.length);
            count = 0;
            for (int j = 0; j < min; j++) {
                if (ch1[j] == ch2[j]) {
                    count++;
                } else {
                    break;
                }
            }
            if (count == 0) {
                return "";
            }
        }
        String str = "";
        char[] res = strs[0].toCharArray();
        for (int i = 0; i < count; i++) {
            str = str + res[i];

        }
        return str;
    }

    public static void main(String[] args) {
        String strs[] = new String[] { "aaa", "aa", "aaaba" };
        longestCommonPrefix(strs);

    }
}