import java.util.Arrays;

class Solution {
    static long[] productExceptSelf(int nums[], int n) {
        // code here
        long p[] = new long[n];

        for (int i = 0; i < n; i++) {
            long prod = 1;

            for (int j = 0; j < n; j++) {
                if (i != j) {
                    prod = prod * nums[j];
                }
            }
            p[i] = prod;
        }
        return p;
    }

    public static void main(String[] args) {
        int nums[] = new int[] { 10, 3, 5, 6, 2 };

        long num[] = productExceptSelf(nums, nums.length);
        System.out.println(Arrays.toString(num));
    }
}
