
class Solution {
    public static long zeroFilledSubarray(int[] nums) {

        int sum = 0;
        int len = 0;
        long count = 0;
        for (int i = 0; i < nums.length; i++) {
            sum = sum + nums[i]; // calculate the sum of elements

            if (sum != 0) { // the sum is not 0 then reset the sum and len to 0
                sum = 0;
                len = 0;
            } else {
                len++;
                count = count + len;
            }
        }
        return count;
    }

    public static void main(String[] args) {
        int nums[] = { 1, 2, 0, 0, 3, 4, 0, 0, 4 };
        System.out.println(zeroFilledSubarray(nums));
    }
}