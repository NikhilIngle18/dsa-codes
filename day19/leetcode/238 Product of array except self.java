
// Given an integer array nums, return an array answer such that answer[i] is equal to the product of all the elements of nums except nums[i].

// The product of any prefix or suffix of nums is guaranteed to fit in a 32-bit integer.

// You must write an algorithm that runs in O(n) time and without using the division operation.

// Example 1:

// Input: nums = [1,2,3,4]
// Output: [24,12,8,6]
// Example 2:

// Input: nums = [-1,1,0,-3,3]
// Output: [0,0,9,0,0]

import java.util.Arrays;

class Solution {
    public static int[] productExceptSelf(int[] nums) {
        int ans[] = new int[nums.length];
        int suf = 1;
        int pre = 1;

        for (int i = 0; i < nums.length; i++) {
            ans[i] = pre;
            pre = pre * nums[i];
        }

        for (int i = nums.length - 1; i >= 0; i--) {
            ans[i] = suf * ans[i
            ];
            suf = suf * nums[i];
        }
        return ans;
    }

    public static void main(String[] args) {
        int nums[] = { 1, 2, 3, 4 };
        int ans[] = productExceptSelf(nums);
        System.out.println(Arrays.toString(ans));
    }
}
