package Leetcode;
import java.util.Arrays;

class Solution {
    public static void sortColors(int[] nums) {
        int c0 = 0;
        int c1 = 0;
        int c2 = 0;

        // count the occurance of 0,1and 2

        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == 0) {
                c0++;
            } else if (nums[i] == 1) {
                c1++;
            } else {
                c2++;
            }
        }

        // according to the count first add 0, and then 1 and then 2
        for (int i = 0; i < nums.length; i++) {
            if (i < c0)
                nums[i] = 0;
            else if (i >= c0 && i < c0 + c1)
                nums[i] = 1;
            else
                nums[i] = 2;
        }

        // ## another logic ##
        // int i=0;
        // while(c0!=0){ //adding 0
        // nums[i]=0;
        // i++;
        // c0--;
        // }
        // while(c1!=0){ //adding 1
        // nums[i]=1;
        // i++;
        // c1--;
        // }
        // while(c2!=0){ // adding 2
        // nums[i]=2;
        // i++;
        // c2--;
        // }

        System.out.println(Arrays.toString(nums));

    }

    public static void main(String[] args) {
        int nums[] = { 2, 0, 2, 1, 1, 0 };
        sortColors(nums);
    }
}