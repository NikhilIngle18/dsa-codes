package Leetcode;

class Solution {
    public static int[] rowAndMaximumOnes(int[][] arr) {
        int count1 = 0;
        int max = 0;
        int idx = 0;
        int[] ans = new int[2];

        for (int i = 0; i < arr.length; i++) {
            count1 = 0;
            for (int j = 0; j < arr[i].length; j++) { // count the occurence of 1,s in each row
                if (arr[i][j] == 1) {
                    count1++;
                }

            }
            if (count1 > max) { // compare the count with maximum count ,if it is greater than max the store the
                                // index
                max = count1;
                idx = i;
            }
        }
        ans[0] = idx;
        ans[1] = max;
        return ans;
    }

    public static void main(String[] args) {
        int arr[][] = { { 0, 1, 1, 1 },
                { 0, 0, 1, 1 },
                { 1, 1, 1, 1 },
                { 0, 0, 0, 0 } };

        int m = 4, n = 4;
        rowAndMaximumOnes(arr);
    }
}