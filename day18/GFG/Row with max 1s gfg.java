package GFG;
class Solution {
    static int rowWithMax1s(int arr[][], int n, int m) {
        int count1 = 0;
        int max = 0;
        int idx = -1;

        for (int i = 0; i < n; i++) {
            count1 = 0;
            for (int j = 0; j < m; j++) { // count the occurence of 1,s in each row
                if (arr[i][j] == 1) {
                    count1++;
                }

            }
            if (count1 > max) { // compare the count with maximum count ,if it is greater than max the store the
                                // index
                max = count1;
                idx = i;
            }
        }
        return idx;
    }

    public static void main(String[] args) {
        int arr[][] = { { 0, 1, 1, 1 },
                { 0, 0, 1, 1 },
                { 1, 1, 1, 1 },
                { 0, 0, 0, 0 } };

        int m = 4, n = 4;
        System.out.println(rowWithMax1s(arr, n, m));

    }
}