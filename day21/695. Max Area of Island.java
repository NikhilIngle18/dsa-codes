
class Solution {
    int count = 0;

    void island(int[][] g, int i, int j) {
        if (i < 0 || j < 0 || i >= g.length || j >= g[0].length || g[i][j] == 0)
            return;
        g[i][j] = 0;
        count++;
        island(g, i + 1, j);// above

        island(g, i - 1, j);// below
        island(g, i, j + 1);// right
        island(g, i, j - 1);// left
    }

    public int maxAreaOfIsland(int[][] grid) {
        int max = 0;
        for (int i = 0; i < grid.length; i++)
            for (int j = 0; j < grid[0].length; j++)
                if (grid[i][j] == 1) {
                    count = 0;
                    island(grid, i, j);
                    max = Math.max(max, count); // find max count of 1

                }
        return max;
    }

}
