import java.util.ArrayList;

class Solution {
    static int removeDuplicates(int[] nums) {
        int n = nums.length;
        ArrayList<Integer> al = new ArrayList<>();

        for (int i = 0; i < n; i++) {
            if (al.contains(nums[i]) == false) {
                al.add(nums[i]);
            }

        }

        int k = al.size();
        int i = 0;
        while (i < k) {
            nums[i] = al.get(i);
            i++;

        }
        return k;
    }

    public static void main(String[] args) {
        int nums[] = new int[] { 1, 1, 2, 3, 3, 3, 4, 5, 5 };
        int k = removeDuplicates(nums);
        for (int i = 0; i < k; i++) {
            System.out.println(nums[i]);
        }
    }
}