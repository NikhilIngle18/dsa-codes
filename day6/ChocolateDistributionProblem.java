import java.util.*;

class Solution {
    public long findMinDiff(ArrayList<Integer> a, int n, int m) {

        Collections.sort(a);
        int r = m - 1;
        int l = 0;
        long res = Integer.MAX_VALUE;
        if (n == m) {
            return a.get(n - 1) - a.get(0);
        }
        while (r < n) {
            res = Math.min(res, a.get(r) - a.get(l));
            r++;
            l++;
        }
        return res;
    }
}