import java.util.Arrays;

class Solution {
    static int maximumProduct(int[] nums) {
        int n = nums.length;
        Arrays.sort(nums);
        int r = nums[n - 1] * nums[n - 2] * nums[n - 3];
        int l = nums[0] * nums[1] * nums[n - 1];
        if (l < r) {
            return r;
        } else {
            return l;
        }

        // GFG SOLUTION

        // if(n<3){
        // return -1;
        // }
        // Arrays.sort(arr);
        // long a1=arr[0];
        // long a2=arr[1];
        // long a3=arr[n-1];
        // long b1=arr[n-3];
        // long b2=arr[n-2];
        // long b3=arr[n-1];
        // return Math.max(a1*a2*a3,b1*b2*b3);

    }

    public static void main(String[] args) {
        int nums[] = new int[] { 10, 3, 5, 6, 20 };
        int k = maximumProduct(nums);

        System.out.println(k);

    }
}