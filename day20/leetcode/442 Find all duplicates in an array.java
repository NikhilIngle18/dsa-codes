import java.util.*;

class Solution {
    public static List<Integer> findDuplicates(int[] nums) {
        List<Integer> ans = new ArrayList<>();
        Arrays.sort(nums);
        for (int i = 1; i < nums.length; i++) {
            if (nums[i - 1] == nums[i]) {
                ans.add(nums[i]);
            }

        }
        return ans;
    }

    public static void main(String[] args) {
        int nums[] = { 1, 3, 2, 2, 4, 5, 5 };
        List<Integer> ans = findDuplicates(nums);
    }
}