class Solution {
    public static int findDuplicate(int[] nums) {
        Arrays.sort(nums);

        for (int i = 1; i < nums.length; i++) {
            if (nums[i - 1] == nums[i]) {
                return nums[i];
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        int nums = { 1, 2, 3, 4, 2 };
        System.out.println(findDuplicate(nums));
    }
}
